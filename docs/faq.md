# Frequently asked Questions

## Windows Compatibility

### Known Issues

- `loki` log-driver isn't supported on Windows because `Docker doesn't support plugins on this platform`
- There are issues with Windows Server versions other than 1903 und Kubernetes 1.17 with Flannel