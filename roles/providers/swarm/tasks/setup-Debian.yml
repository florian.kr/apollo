- name: retrieving swarm status
  shell: |
    set -o pipefail
    docker info | egrep 'Swarm: ' | cut -d ' ' -f3
  register: swarm_status
  changed_when: False
  args:
    executable: /bin/bash

- name: setting swarm facts
  set_fact:
    swarm: "{{ swarm_status }}"
 
- name: setting node facts
  set_fact:  
    advertise_address: "{{ hostvars[inventory_hostname]['cluster_ip'] }}:2377"
    manager_address: "{{ apollo_manager }}:2377"

- name: creating dynamic swarm groups
  group_by:
    key: swarm_{{ item }}_{{ swarm.stdout }}
  when: "item in group_names"
  with_items:
    - manager
    - worker

- name: initializing swarm cluster
  shell: >
    docker swarm init
    --advertise-addr={{ advertise_address }}
  when: "'swarm_manager_active' not in groups and inventory_hostname == groups['manager'][0]"
  register: bootstrap_first_node

- name: adding initialized host to swarm_manager_active group # noqa 403 503
  add_host:
    hostname: "{{ ansible_hostname }}"
    groups: swarm_manager_active
  when: bootstrap_first_node.changed and inventory_hostname == groups['manager'][0]
  register: swarm_manager_active

- name: setting node labels # noqa 403 503
  shell: >
    docker node update --label-add role=manager --label-add environment={{ arc['space']['name'] }} --label-add space={{ arc['space']['name'] }} --label-add provider={{ arc['infrastructure']['provider'] }} {{ ansible_hostname }}
  when: swarm_manager_active is defined and swarm_manager_active.changed and inventory_hostname == groups['manager'][0]

- name: retrieving swarm manager token
  shell: docker swarm join-token -q manager # noqa 305
  register: swarm_manager_token
  changed_when: False
  when: "'swarm_manager_active' in groups and inventory_hostname == groups['manager'][0]"

- name: retrieving swarm worker token # noqa 305
  shell: docker swarm join-token -q worker
  register: swarm_worker_token
  changed_when: False
  when: "'swarm_manager_active' in groups and inventory_hostname == groups['manager'][0]"

- name: joining manager nodes to cluster # noqa 301
  shell: >
    docker swarm join
    --advertise-addr={{ advertise_address }}
    --token={{ hostvars[groups['manager'][0]]['swarm_manager_token']['stdout'] }}
    {{ manager_address }}
  when: "'swarm_manager_inactive' in group_names and 'swarm_manager_active' not in group_names and inventory_hostname != groups['manager'][0]"
  register: swarm_manager_joined

- name: setting node labels # noqa 301
  shell: >
    docker node update --label-add role=manager --label-add environment={{ arc['space']['name'] }} --label-add space={{ arc['space']['name'] }} --label-add provider={{ arc['infrastructure']['provider'] }} {{ ansible_hostname }}
  when: swarm_manager_joined.changed and inventory_hostname != groups['manager'][0]

- name: joining worker nodes to cluster # noqa 301
  shell: >
    docker swarm join
    --advertise-addr={{ advertise_address }}
    --token={{ hostvars[groups['manager'][0]]['swarm_worker_token']['stdout'] }}
    {{ manager_address }}
  when: "'swarm_worker_inactive' in group_names and 'swarm_worker_active' not in group_names and inventory_hostname != groups['manager'][0]"
  register: swarm_worker_joined

- name: setting node labels # noqa 301
  shell: >
    docker node update --label-add role=worker --label-add environment={{ arc['space']['name'] }} --label-add space={{ arc['space']['name'] }} --label-add provider={{ arc['infrastructure']['provider'] }} {{ ansible_hostname }}
  when: swarm_worker_joined.changed and inventory_hostname != groups['manager'][0]
  delegate_to: "{{ groups['manager'][0] }}"